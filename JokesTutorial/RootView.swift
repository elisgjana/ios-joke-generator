//
//  ContentView.swift
//  JokesTutorial
//
//  Created by Elis Gjana on 14.10.21.
//

import SwiftUI
import ComposableArchitecture

struct RootState: Equatable{
    var joke = JokeState()
    var favorites = FavoritesState(favorites: [])
}

enum RootAction: Equatable {
    case joke(JokeAction)
    case favorites(FavoritesAction)
    case onAppear
}

struct RootEnvironment{
    var joke: JokeClient
    var mainQueue: AnySchedulerOf<DispatchQueue>
}

let rootReducer = Reducer<RootState, RootAction, RootEnvironment>.combine(
    .init { state, action, _ in
        
        switch action{
        case .onAppear:
            state.favorites.favorites = state.joke.favoriteJokes
            return .none
        case .joke(_):
            
            return .none
        }
    },
    
    jokeReducer.pullback(
        state: \.joke,
        action: /RootAction.joke,
        environment: { .init(joke: $0.joke, mainQueue: $0.mainQueue) }
    ),
    
    favoritesReducer.pullback(
        state: \.favorites,
        action: /RootAction.favorites,
        environment: { _ in .init() }
    )
    
).debug()

struct RootView: View {
    
    let store: Store<RootState, RootAction>
    
    var body: some View {
        WithViewStore(self.store.stateless) { viewStore in
            NavigationView{
                Form{
                    Section(header: Text("Jokes Menu")) {
                        NavigationLink(
                            "Joke Generator",
                            destination: JokeView(
                                store: self.store.scope(
                                    state: \.joke,
                                    action: RootAction.joke
                                )
                            )
                        )
                        
                        NavigationLink(
                            "Favorite Jokes",
                            destination: FavoritesView(
                                store: self.store.scope(
                                    state: \.favorites,
                                    action: RootAction.favorites
                                )
                            )
                        )
                    }
                }
                .navigationBarTitle("Jokes Tutorial")
                .onAppear { viewStore.send(.onAppear) }
            }
        }
    }
}

struct RootView_Previews: PreviewProvider {
    static var previews: some View {
        RootView(
            store: Store(
                initialState: RootState(),
                reducer: rootReducer,
                environment: RootEnvironment(
                    joke: .live,
                    mainQueue: .main
                )
            )
        )
    }
}
