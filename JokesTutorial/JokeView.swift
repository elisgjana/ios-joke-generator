//
//  JokeView.swift
//  JokesTutorial
//
//  Created by Elis Gjana on 14.10.21.
//

import SwiftUI
import ComposableArchitecture

struct JokeState: Equatable {
    var joke: Joke?
    var favoriteJokes = [Joke]()
}

enum JokeAction: Equatable {
    case jokeButtonTapped
    case jokeResponse(Result<Joke?, JokeClient.Error>)
    case saveJokeButtonTapped
    case removeJokeButtonTapped
}

struct JokeEnvironment {
    var joke: JokeClient
    var mainQueue: AnySchedulerOf<DispatchQueue>
}

let jokeReducer = Reducer<JokeState, JokeAction, JokeEnvironment> { state, action, environment in
    
    switch action{
    case .jokeButtonTapped:
        return environment.joke.fetch()
            .receive(on: environment.mainQueue)
            .catchToEffect(JokeAction.jokeResponse)
    case let .jokeResponse(.success(response)):
        state.joke = response
        return .none
    case let .jokeResponse(.failure(error)):
        return .none
    case .saveJokeButtonTapped:
        guard let joke = state.joke else { return .none }
        state.favoriteJokes.append(joke)
        return .none
    case .removeJokeButtonTapped:
        guard let joke = state.joke else { return .none }
        
        state.favoriteJokes.removeAll(where: { $0 == joke })
        
        return .none
    }
}

struct JokeView: View {
    let store: Store<JokeState, JokeAction>
    
    var body: some View{
        WithViewStore(self.store) { viewStore in
            Form{
                if let joke = viewStore.joke{
                    Text("JOKE \(joke.id)")
                    Text(joke.setup)
                    Text(joke.delivery)
                }
            }
            .navigationBarTitle("Joke Generator")
            .navigationBarItems(
                trailing:
                    HStack{
                        if let joke = viewStore.joke{
                            if viewStore.favoriteJokes.contains(joke){
                                Button("Remove") { viewStore.send(.removeJokeButtonTapped) }
                            }else{
                                Button("Save") { viewStore.send(.saveJokeButtonTapped) }
                            }
                        }else{
                            Button("Save") {}
                        }
                        Button("New") { viewStore.send(.jokeButtonTapped) }
                        
                    }
            )
        }
    }
}

struct JokeView_Previews: PreviewProvider {
    static var previews: some View {
        JokeView(
            store: Store(
                initialState: JokeState(),
                reducer: jokeReducer,
                environment: JokeEnvironment(
                    joke: .live,
                    mainQueue: .main
                )
            )
        )
    }
}
