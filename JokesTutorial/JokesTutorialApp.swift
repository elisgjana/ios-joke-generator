//
//  JokesTutorialApp.swift
//  JokesTutorial
//
//  Created by Elis Gjana on 14.10.21.
//

import SwiftUI
import ComposableArchitecture

@main
struct JokesTutorialApp: App {
    var body: some Scene {
        WindowGroup {
            RootView(
                store: Store(
                    initialState: RootState(),
                    reducer: rootReducer,
                    environment: RootEnvironment(
                        joke: .live,
                        mainQueue: .main
                    )
                )
            )
        }
    }
}
