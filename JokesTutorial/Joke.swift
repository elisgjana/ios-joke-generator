//
//  Joke.swift
//  JokesTutorial
//
//  Created by Elis Gjana on 14.10.21.
//

import Foundation

struct Joke: Decodable, Equatable{
    var setup: String = "Error Title"
    var delivery: String = "Error Description"
    var id: Int = 0
    
    init(setup: String, delivery: String, id: Int){
        self.setup = setup
        self.delivery = delivery
        self.id = id
    }
}
