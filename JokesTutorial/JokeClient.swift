//
//  JokeClient.swift
//  JokesTutorial
//
//  Created by Elis Gjana on 14.10.21.
//

import Combine
import ComposableArchitecture
import XCTestDynamicOverlay

struct JokeClient {
  var fetch: () -> Effect<Joke?, Error>

  struct Error: Swift.Error, Equatable {}
}

// This is the "live" fact dependency that reaches into the outside world to fetch trivia.
// Typically this live implementation of the dependency would live in its own module so that the
// main feature doesn't need to compile it.
extension JokeClient {
    static let live = Self(
      fetch: {
        URLSession.shared.dataTaskPublisher(
          for: URL(string: "https://v2.jokeapi.dev/joke/programming")!
        )
        .map { data, _ in data }
        .decode(type: Joke?.self, decoder: JSONDecoder())
        .catch { _ in
          Just(nil)
            .delay(for: 1, scheduler: DispatchQueue.main)
        }
        .setFailureType(to: Error.self)
        .eraseToEffect()
      })
}
