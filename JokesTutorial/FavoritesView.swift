//
//  FavoritesView.swift
//  JokesTutorial
//
//  Created by Elis Gjana on 14.10.21.
//

import SwiftUI
import ComposableArchitecture

struct FavoritesState: Equatable{
    var favorites: [Joke]
}

enum FavoritesAction: Equatable{
    
}

struct FavoritesEnvironment {}


let favoritesReducer = Reducer<FavoritesState, FavoritesAction, FavoritesEnvironment> { state, action, _ in
    return .none
}

struct FavoritesView: View {
    
    let store: Store<FavoritesState, FavoritesAction>
    
    var body: some View{
        WithViewStore(self.store) { viewStore in
            List {
                ForEach(viewStore.favorites, id: \.id) { joke in
                    
                    NavigationLink(
                        "\(joke.id). \(joke.setup)",
                        destination: FavoriteDetailsView(title: joke.setup, description: joke.delivery)
                    )
              }
            }
            .navigationBarTitle("Favorite Jokes")
        }
    }
}

struct FavoritesView_Previews: PreviewProvider {
    static var previews: some View {
        FavoritesView(
            store: Store(
                initialState: FavoritesState(
                    favorites: [
                        Joke(setup: "Joke 1 Title", delivery: "Joke 1 Description", id: 1),
                        
                        Joke(setup: "Joke 2 Title", delivery: "Joke 2 Description", id: 2),
                        
                        Joke(setup: "Joke 3 Title", delivery: "Joke 3 Description", id: 3)
                    ]
                ),
                reducer: favoritesReducer,
                environment: FavoritesEnvironment()
            )
        )
    }
}
