//
//  FavoriteDetailView.swift
//  JokesTutorial
//
//  Created by Elis Gjana on 15.10.21.
//

import SwiftUI
import ComposableArchitecture

struct FavoriteDetailsView: View{
    
    @State var title: String
    @State var description: String
    
    var body: some View{
    
        VStack {
            Text(title)
            Text(description)
        }
    }
}
